package assigment04;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class assig4 {
	
	private WebDriver driver;
	
	@Before
	public void setUp() {
		
	System.setProperty("webdriver.chrome.driver","./src/main/resources/driver/chromedriver.exe");
	driver = new ChromeDriver();
	driver.get("https://formy-project.herokuapp.com/form");
	}
	
	@Test
	public void testPage() {
		startUp page  = new startUp(driver);
		String FirstName = "Luis";
		String LastName = "Adame";
		String JobTitle = "Electronics Engineer";
		page.setInfo(FirstName, LastName, JobTitle);

		//Step 3
		page.SetSub();
		
		//Step 4
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		
		//Step 5
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
		System.out.println("Test finished");


	}


}
