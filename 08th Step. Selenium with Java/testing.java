package com.assignment5;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class testing {
	WebDriver driver;
	String firstName, lastName, jobTitle;
	ReadExcelFile Inf;
	@Before
	public void prep() {
		driver = new ChromeDriver();
		System.setProperty("webdriver.chrome.driver","./src/main/resources/driver/chromedriver.exe");
		driver.get("https://formy-project.herokuapp.com/form");
		Inf = new ReadExcelFile("C:\\Users\\Luis David Adame\\Desktop\\TCS\\Automation Workshop\\08th Step. Selenium with Java\\assignment05\\src\\main\\resources\\excel\\Information.xlsx");
		}
	@Test
	public void fill() {		
		driver.findElement(By.xpath("//input[@id ='first-name']")).sendKeys(Inf.getData(0, 0, 1));
	    driver.findElement(By.xpath("//input[@id ='last-name']")).sendKeys(Inf.getData(0, 1, 1));
	    driver.findElement(By.xpath("//input[@id ='job-title']")).sendKeys(Inf.getData(0, 2, 1));
	    driver.findElement(By.cssSelector("#radio-button-2")).click();
	    driver.findElement(By.cssSelector("#checkbox-2")).click();
	    driver.findElement(By.cssSelector("option[value='1']")).click();
	    driver.findElement(By.cssSelector("#datepicker")).click();
	    driver.findElement(By.cssSelector(".today.day")).click();
		driver.findElement(By.cssSelector("a[role='button']")).click();
		
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
		System.out.println("Test finished");
	}
	

}
