package fibonacci;

public class fibonacci {
    public static int fibRec(int n){
        if(n == 0)
            return 0;
        else if(n == 1 )
            return 1;
        else
            return fibRec(n-1) + fibRec(n-2);
    }
    public static void PrintFib(int num){
        int[] fibArr = new int [num+2];
        for(int i =2;i<num+1;i++){
            int fibNum = fibRec(i);
            fibArr[i] = fibNum;
            System.out.print(fibArr[i]);
        }
    }
    public static void main(String args[]) {
        int maxNumber = 6;
        PrintFib(maxNumber);
    }

}
