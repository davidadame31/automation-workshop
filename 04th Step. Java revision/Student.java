package Student;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
public class Student {
    double studentID;
    String firstName;
    String middleName;
    String lastName;
    String DOB;

    public Student(double studentID, String firstName, String middleName, String lastName, String DOB){
        this.studentID = studentID;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.DOB = DOB;
    }

    public double getStudentID() {
        return studentID;
    }
    public String getName() {
        return firstName+" "+middleName+" "+lastName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        String oldstring = DOB + " 00:00:00.0" ;
        LocalDateTime datetime = LocalDateTime.parse(oldstring, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.S"));
        String newstring = datetime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        this.DOB = newstring;
    }
    public static void main(String[] args) {
        Student student1 = new Student(Math.random(), "Luis ", "David", "Adame", "17/06/1998");
        student1.setDOB(student1.DOB);
        Student student2 = new Student(Math.random(), "Ricardo ", "", "Adame", "28/10/1972");
        student2.setDOB(student2.DOB);
        Student student3 = new Student(Math.random(), "Daniel ", "Alejandro", "Pasos", "20/01/1998");
        student3.setDOB(student3.DOB);
        System.out.println(student1.getName());
        System.out.println(student1.getDOB());
        System.out.println(student2.getName());
        System.out.println(student2.getDOB());
        System.out.println(student3.getName());
        System.out.println(student3.getDOB());
    }
}
