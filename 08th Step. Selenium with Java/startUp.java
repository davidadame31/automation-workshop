package assigment04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class startUp {
	protected WebDriver driver;
	
	private By firstNameBy = By.xpath("//input[@id ='first-name']");
	private By lastNameBy = By.xpath("//input[@id ='last-name']");
	private By jobTitleBy = By.xpath("//input[@id ='job-title']");
	
	private By educationLevelBy = By.cssSelector("#radio-button-2");
	private By sexBy = By.cssSelector("#checkbox-2");
	private By yearsBy = By.cssSelector("option[value='1']");
	private By dateBy = By.cssSelector("#datepicker");
	private By dateTodayBy = By.cssSelector(".today.day");
	
	private By submitBy = By.cssSelector("a[role='button']");
	
	public startUp(WebDriver driver){
		this.driver = driver;
	}
	
	public void setInfo(String firstName, String lastName, String jobTitle){
	    driver.findElement(firstNameBy).sendKeys(firstName);
	    driver.findElement(lastNameBy).sendKeys(lastName);
	    driver.findElement(jobTitleBy).sendKeys(jobTitle);
	    
	    driver.findElement(educationLevelBy).click();
	    driver.findElement(sexBy).click();
	    driver.findElement(yearsBy).click();
	    driver.findElement(dateBy).click();
	    driver.findElement(dateTodayBy).click();
	}
	public void SetSub() {
		driver.findElement(submitBy).click();
	}
}