public class assignment3 {
    public static void main(String[] args) {
        getFib(8);
        getFib(25);

    }
    static void getFib(int num) {
        System.out.println("The Fibonacci serie of "+num+" is: ");
        int num1 = 0, num2 = 1;

        int counter = 0;

        // Iterate till counter is N
        while (counter < num) {

            // Print the number
            System.out.print(num1 + " ");

            // Swap
            int num3 = num2 + num1;
            num1 = num2;
            num2 = num3;
            counter = counter + 1;
        }
        System.out.println(" ");
    }
}
